package inheritance;

public class Book {
    protected String title;
    private String author; //access via getters

    //constructor
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    //getters
    public String getTitle() {
        return this.title;
    }
    public String getAuthor() {
        return this.author;
    }

    //toString
    public String toString() {
        return "Title: " + this.title + "; Author: " + this.author;
    }
}