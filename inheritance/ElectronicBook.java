package inheritance;

public class ElectronicBook extends Book {
    int numberBytes;

    //construuctor
    public ElectronicBook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    //getters
    public int getNumberBytes() {
        return this.numberBytes;
    }

    //toString
    public String toString() {
        String fromBase = super.toString(); //access toString from Book
        fromBase += ", Bytes: " + this.numberBytes;
        return fromBase;
    }
}
