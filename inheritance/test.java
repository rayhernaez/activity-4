package inheritance;

public class test {
    public static void main(String[] args) {

        Book book = new Book ("First Title", "First Author");
        System.out.println(book);
        
        ElectronicBook ebook = new ElectronicBook("Second Title", "Second Author", 5);
        System.out.println(ebook);

    }
}