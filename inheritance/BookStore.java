package inheritance;

public class BookStore {

    public static void main(String[] args) {

        Book[] books = new Book[5];
        books[0] = new Book("First Title", "First Author");
        books[1] = new ElectronicBook("Second Title", "Second Author", 2);
        books[2] = new Book("Third Title", "Third Author");
        books[3] = new ElectronicBook("Fourth Title", "Fourth Author", 4);
        books[4] = new ElectronicBook("Fifth Title", "Fifth Author", 5);

        for (Book a : books) {
            System.out.println(a);
        }

        //When you printed the 1st Book of the array, which toString() method was called?
        // -> toString() from Book.java
        System.out.println(books[0]);

        //When you printed the 2nd Book of the array, which toString() method was called
        // -> toString() from Book.java
        System.out.println(books[1]);

        //The first Book in the array actually is pointing at a Book.
        //Try calling the method getNumberOfBytes() on this object.
        //What happens? If there is an error, then what sort of error is there?
        //System.out.println(books[0].getNumberBytes()); //method call is undefined

        //The second Book in the array actually is pointing at an ElectronicBook.
        //Try calling the method getNumberOfBytes() on this object. 
        //What happens? If there is an error, then what sort of error is there?
        //books[1].getNumberBytes(); //undefined method

        //The second Book in the array is actually pointing at an ElectronicBook.
        //Try assigning the value to a variable of type ElectronicBook.
        //What happens? If there is an error, then what sort of error is there?
        //ElectronicBook b = books[1]; //type mismatch

        //What happens if you add a cast to cast books[1] to an ElectronicBook? 
        //If there is an error, then what sort of error is there?
        ElectronicBook b = (ElectronicBook)books[1];
        System.out.println(b);

        //Follow up to the previous question:
        //What happens if you try to call the getNumberOfBytes() method on the object b?
        //If there is an error, then what sort of error is there?
        System.out.println(b.getNumberBytes()); //no error

        //Repeat the last 2 steps again, but do them on books[0], 
        //which is actually pointing at a Book. What happens? 
        //If there is an error, then what sort of error is there?

        ElectronicBook c = (ElectronicBook)books[0];
        System.out.println(c); //Exception error
        System.out.println(c.getNumberBytes()); //Exception error

    }
}
